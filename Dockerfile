# ARG BUILD_FROM
# FROM ${BUILD_FROM}


FROM python:3.9-slim-buster

WORKDIR /usr/src

COPY requirements.txt .

# RUN \
#     pip3 install --no-cache-dir --no-index --only-binary=:all: \
#     -r requirements.txt --use-deprecated=legacy-resolver

#RUN pip3 install --find-links "https://wheels.hass.io" \
#	python3-wheel pyeventbus3 pyserial pynmeagps flask throttle

#RUN apt-get update -yqq \
#    	&& apt-get upgrade -yqq \
#    	&& apt-get install -yqq --no-install-recommends \
#	apt-utils \
#	build-essential \
#	libffi-dev \
#	libpq-dev \
#	libssl-dev \
#	python3-pandas \
#	python3-numpy

ENV PYTHONPATH="${PYTHONPATH}:/usr/lib/python3/dist-packages"

ENV PIP_EXTRA_INDEX_URL=https://www.piwheels.org/simple

RUN pip install -U pip setuptools wheel && \
    pip install pyeventbus3 pyserial pynmeagps flask throttle

COPY carpilot ./carpilot

WORKDIR .

CMD python -u -m carpilot