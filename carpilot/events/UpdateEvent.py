from datetime import datetime

class UpdateEvent:
    def __init__(self, componentid, state, attributes):
        self.id = componentid
        self.state = state
        self.attributes = attributes
        self.last_update = datetime.now().strftime("%Y-%m-%d %H:%M:%S.%s")

    def getId(self):
        return self.id

    def getState(self):
        return self.state

    def getAttributes(self):
        return self.attributes

    def getLastUpdate(self):
        return self.last_update;
