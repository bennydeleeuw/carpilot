from .LoggingEventListener import LoggingEventListener
from .LatestStateEventListener import LatestStateEventListener

ll = LoggingEventListener()
ll.register(ll)

lsev = LatestStateEventListener()
lsev.register(lsev)
