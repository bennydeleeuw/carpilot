from .EventListener import EventListener
from carpilot.repositories import stateRepository


class LatestStateEventListener(EventListener):

    def __init__(self):
        self.states = {}
        self.stateRepository = stateRepository.StateRepository()

    def processEvent(self, event):
        # id = event.getId()
        # self.states[id] = {}
        # self.states[id]["state"] = event.getState()
        # self.states[id]["attributes"] = event.getAttributes()

        self.stateRepository.setState(event.getId(), event.getState(), event.getAttributes(), event.getLastUpdate())

        # print(json.dumps(self.states))

        # f = open("logging.csv", "a")
        # f.write(data + "\n")
        # f.close()
