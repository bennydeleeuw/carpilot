from carpilot.events import UpdateEvent
from pyeventbus3.pyeventbus3 import *


class EventListener:

    def __init__(self):
        self.task = None

    def register(self, aInstance):
        PyBus.Instance().register(aInstance, self.__class__.__name__)

    @subscribe(threadMode=Mode.BACKGROUND, onEvent=UpdateEvent)
    def readFromEvent(self, event):
        self.processEvent(event)

    def processEvent(self, event):
        pass
