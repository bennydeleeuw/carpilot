import json
import throttle
from .EventListener import EventListener
from datetime import datetime


class LoggingEventListener(EventListener):

    # @throttle.wrap(5, 1) #cannot do this, might miss events, should be on another level
    def processEvent(self, event):
        filename = datetime.now().strftime("%Y-%m-%d") + ".log"
        # data = json.dumps(event.__dict__)
        # f = open("log/" + filename, "a")
        # f.write(data + "\n")
        # f.close()
