class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class StateRepository(object):
    __metaclass__ = Singleton

    states = {}

    def setState(self, name, state, attributes, last_update):
        id = name
        self.states[id] = {}
        self.states[id]["state"] = state
        self.states[id]["attributes"] = attributes
        self.states[id]["last_update"] = last_update

    def getStates(self):
        return self.states
