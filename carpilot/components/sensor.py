from carpilot.events import UpdateEvent
from pyeventbus3.pyeventbus3 import *


class Sensor:

    def __init__(self):
        self.update_interval = 1
        self.id = None
        self.state = None
        self.attributes = {}
        self.running = True

    def init(self):
        pass

    def update(self):
        pass

    def update_loop(self):
        while True:
            self.update()
            self.update_state()
            time.sleep(self.update_interval)

    def update_state(self):
        PyBus.Instance().post(UpdateEvent(self.id, self.state, self.attributes))
