from carpilot.sensors.gpssensor import GpsSensor


def init_components():
    # return [CpuSensor(), GpsSensor(), ObdSensor()]
    return [GpsSensor()]
