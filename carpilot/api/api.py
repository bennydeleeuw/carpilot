import threading
from flask import Flask
from carpilot.repositories import stateRepository


sr = stateRepository.StateRepository()
api = Flask(__name__)


@api.route("/states")
def states():
    return sr.getStates()


def start():
    apithread = threading.Thread(target=lambda: api.run())
    apithread.start()


def stop():
    # Needs to be implemented
    print("Stopping " + __name__)
