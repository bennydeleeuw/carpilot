from pyeventbus3.pyeventbus3 import *

PyBus.Configure(conf={'max_threads': 5})

from carpilot.components import components_helper
import carpilot.listeners


def main():
    # api.start()

    components = components_helper.init_components()

    for c in components:
        c.init()
        t = threading.Thread(target=c.update_loop).start()

        # loop.create_task(c.update_loop())
    #
    # try:
    #     loop.run_forever()
    # except KeyboardInterrupt:
    #     print("Process interrupted")
    # finally:
    #     loop.close()
    #     print("Successfully shutdown the Mayhem service.")


if __name__ == "__main__":
    main()
