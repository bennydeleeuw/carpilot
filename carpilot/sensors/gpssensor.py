from carpilot.components.sensor import Sensor
from serial import Serial
from pynmeagps import NMEAReader


class GpsSensor(Sensor):

    def __init__(self):
        Sensor.__init__(self)
        self.id = "GpsSensor"
        self.update_interval = 1
        self.nmr = None
        self.stream = None

    def init(self):
        self.state = None
        self.attributes = None
        try:
            #/dev/cu.usbserial-141240
            self.stream = Serial('/dev/gps', 9600, timeout=3)
            self.nmr = NMEAReader(self.stream)
        except Exception as ex:
            print("cannot init GPS")
            print(ex)

    def update(self):

        try:
            (raw_data, msg) = self.nmr.read()
        except:
            print("An exception occurred")
            self.init()
            return

        if msg is None:
            return

        # print(msg)
        # print(msg.msgID)

        if msg.msgID == "GGA":
            self.attributes = {}
            self.attributes["lon"] = msg.lon
            self.attributes["lat"] = msg.lat
            self.attributes["alt"] = msg.alt
            self.attributes["hdop"] = msg.HDOP
            # self.attributes["time"] = msg.time
            print(self.attributes)
            print(msg)

        self.state = "OK"
