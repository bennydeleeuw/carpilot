from carpilot.components.sensor import Sensor
import psutil


class CpuSensor(Sensor):

    def __init__(self):
        Sensor.__init__(self)
        self.id = "CpuSensor"

    def update(self):
        self.state = "OK"
        self.attributes["cpu_percent"] = psutil.cpu_percent()
        # print("cpu")
