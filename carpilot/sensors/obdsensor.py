import obd

from carpilot.components.sensor import Sensor

# import obd
from obd import OBDStatus


class ObdSensor(Sensor):

    def __init__(self):
        Sensor.__init__(self)
        self.connection = None
        self.id = "ObdSensor"

    def init(self):
        pass

    def update(self):
        if self.connection is None or self.connection.status() != OBDStatus.OBD_CONNECTED:
            self.state = None
            self.attributes = {}
            self.connection = obd.OBD()
            return

        cmd = obd.commands.SPEED  # select an OBD command (sensor)

        response = self.connection.query(cmd)  # send the command, and parse the response

        print(response.value)  # returns unit-bearing values thanks to Pint
        print(response.value.to("mph"))  # user-friendly unit conversions

        self.state = "OK"
        self.attributes["xxxx"] = 800
